//
//  String+Extensions.swift
//  HelloAppTvOS
//
//  Created by Federico Malagoni on 22/02/23.
//

import Foundation

extension String {

    func toDate(withFormatter: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = withFormatter
        dateFormatter.timeZone = TimeZone.current
        let date = dateFormatter.date(from: self)
        return date
    }
}
