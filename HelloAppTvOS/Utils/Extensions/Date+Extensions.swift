//
//  Date+Extensions.swift
//  HelloAppTvOS
//
//  Created by Federico Malagoni on 22/02/23.
//

import Foundation

extension Date {

    func toString(withFormatter formatter: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatter
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 3600)
        return dateFormatter.string(from: self)
    }
    
}
