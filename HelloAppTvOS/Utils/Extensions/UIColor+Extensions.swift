//
//  ColorExtensions.swift
//  HelloAppTvOS
//
//  Created by Federico Malagoni on 21/02/23.
//

import UIKit

// MARK: App Colors
extension UIColor {

    static let darkBlue = UIColor.hexStringToUIColor(hex: "065bc9")
    static let blueSky = UIColor.hexStringToUIColor(hex: "56A2CE")
    static let darkRed = UIColor.hexStringToUIColor(hex: "e81c1c")
    static let darkYellow = UIColor.hexStringToUIColor(hex: "ffd105")

}
extension UIColor {

    static func hexStringToUIColor (hex: String) -> UIColor {
        var cString: String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if cString.hasPrefix("#") {
            cString.remove(at: cString.startIndex)
        }
        if cString.count != 6 {
            return UIColor.gray
        }
        var rgbValue: UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
