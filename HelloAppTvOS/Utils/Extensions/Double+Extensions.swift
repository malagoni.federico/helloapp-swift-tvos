//
//  String+Extensions.swift
//  HelloAppTvOS
//
//  Created by Federico Malagoni on 21/02/23.
//

import Foundation

extension Double {

    func convertTo(decimal: Int) -> String {
        let decimalPlaces = decimal

        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = decimalPlaces
        formatter.maximumFractionDigits = decimalPlaces

        return formatter.string(from: self as NSNumber) ?? ""
    }
}
