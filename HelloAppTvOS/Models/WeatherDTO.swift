//
//  WeatherDTO.swift
//  HelloAppTvOS
//
//  Created by Federico Malagoni on 20/02/23.
//

import Foundation

// MARK: - WeatherDTO
struct WeatherDTO: Codable {
    let latitude, longitude, generationtimeMS: Double
    let utcOffsetSeconds: Int
    let timezone, timezoneAbbreviation: String
    let elevation: Int
    let hourlyUnits: HourlyUnits
    let hourly: Hourly

    enum CodingKeys: String, CodingKey {
        case latitude, longitude
        case generationtimeMS = "generationtime_ms"
        case utcOffsetSeconds = "utc_offset_seconds"
        case timezone
        case timezoneAbbreviation = "timezone_abbreviation"
        case elevation
        case hourlyUnits = "hourly_units"
        case hourly
    }
}

extension WeatherDTO {
    struct TemperatureDTO {
        var degree: Double
        var time: String
        func timeFormattedTo(format: String) -> String {
            let date = time.toDate(withFormatter: kDateFormat)
            return date?.toString(withFormatter: format) ?? "N/A"
        }
        var unit: String
    }
    var temperatures: [TemperatureDTO] {
        guard hourly.time.count == hourly.temperature2M.count else {
            return []
        }
        var result: [TemperatureDTO] = []
        for i in 0..<hourly.time.count {
            let temperatureDTO = TemperatureDTO(degree: hourly.temperature2M[i],
                                                time: hourly.time[i],
                                                unit: hourlyUnits.temperature2M)
            result.append(temperatureDTO)
        }
        return result
    }
}

// MARK: - HourlyUnits
struct HourlyUnits: Codable {
    let time, temperature2M: String

    enum CodingKeys: String, CodingKey {
        case time
        case temperature2M = "temperature_2m"
    }
}
// MARK: - Hourly
struct Hourly: Codable {
    let time: [String]
    let temperature2M: [Double]

    enum CodingKeys: String, CodingKey {
        case time
        case temperature2M = "temperature_2m"
    }
}
