//
//  ViewController.swift
//  HelloAppTvOS
//
//  Created by Federico Malagoni on 20/02/23.
//

import UIKit

class ViewHome: UIViewController {

    var collectionView: UICollectionView!

    var viewModel: HomeViewModel = HomeViewModel.init(locationType: .home)

    var cellSpacing: Double = 8.0

    var cellSize: CGSize {
        let width = self.collectionView.frame.width / 3.0 - cellSpacing
        let height = self.collectionView.frame.height / 3.0 - cellSpacing
        return CGSize.init(width: width, height: height)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupLogic()
    }

    func setupLogic() {
        viewModel.itemsRefresh = { [weak self] in
            self?.collectionView.reloadData()
        }
    }

    func setupUI() {
        self.view.backgroundColor = .white

        let collectionViewFlowLayout = UICollectionViewFlowLayout.init()
        collectionView = UICollectionView(frame: CGRect.zero,
                                               collectionViewLayout: collectionViewFlowLayout)

        collectionView.dataSource = self
        collectionView.delegate = self

        collectionView.register(WeatherHomeCell.self,
                                forCellWithReuseIdentifier: WeatherHomeCell.identifier)

        collectionView.backgroundColor = .white
        view.addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
}
// MARK: - Collection view flow delegate
extension ViewHome: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return cellSize
    }

    // MARK: - Space between two cell horizonatally

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {

        return cellSpacing
    }

    // MARK: - Space between two cell horizonatally

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {

        return cellSpacing
    }
}
// MARK: - Collection view data source
extension ViewHome: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.item?.temperatures.count ?? 0
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        guard let item = self.viewModel.item else { return UICollectionViewCell() }
        guard item.temperatures.indices.contains(indexPath.row) else {
            return UICollectionViewCell()
        }

        let temperature = self.viewModel.item!.temperatures[indexPath.row]

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: WeatherHomeCell.identifier, for: indexPath) as? WeatherHomeCell

        cell?.configureFor(temperature: temperature)

        return cell ?? UICollectionViewCell()
    }
}
