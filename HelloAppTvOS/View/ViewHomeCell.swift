//
//  CityWeatherHomeCell.swift
//  HelloAppTvOS
//
//  Created by Federico Malagoni on 20/02/23.
//

import UIKit

class WeatherHomeCell: UICollectionViewCell {

    static let identifier = String(describing: WeatherHomeCell.self)

    let inset: CGFloat = 20

    private let blurEffect = UIBlurEffect(style: .light)
    private var blurView: UIVisualEffectView!
    private let labelCenterTitle = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        labelCenterTitle.text = nil
    }

    func configureFor(temperature: WeatherDTO.TemperatureDTO) {
        
        labelCenterTitle.text = temperature.degree.convertTo(decimal: 2) + temperature.unit + "\n" + temperature.timeFormattedTo(format: "dd/MM/yyyy\nHH:mm")

        switch temperature.degree {
        case ...5:
            blurView.backgroundColor = UIColor.darkBlue
        case 5.nextDown...10:
            blurView.backgroundColor = UIColor.blueSky
        case 0...10:
            blurView.backgroundColor = UIColor.blueSky
        case 10.nextDown...15:
            blurView.backgroundColor = UIColor.darkYellow
        default:
            blurView.backgroundColor = UIColor.darkRed
        }
    }

    func setupUI() {
        blurView = UIVisualEffectView(effect: blurEffect)
        blurView.translatesAutoresizingMaskIntoConstraints = false

        contentView.addSubview(blurView)
        contentView.addSubview(labelCenterTitle)

        NSLayoutConstraint.activate([
            blurView.heightAnchor.constraint(equalTo: contentView.heightAnchor),
            blurView.widthAnchor.constraint(equalTo: contentView.widthAnchor)
        ])

        labelCenterTitle.translatesAutoresizingMaskIntoConstraints = false
        labelCenterTitle.textAlignment = .center
        labelCenterTitle.backgroundColor = .clear
        labelCenterTitle.textColor = UIColor.white
        labelCenterTitle.numberOfLines = 0

        NSLayoutConstraint.activate([
            labelCenterTitle.topAnchor.constraint(equalTo: contentView.topAnchor, constant: inset),
            labelCenterTitle.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: inset),
            labelCenterTitle.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -inset),
            labelCenterTitle.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -inset)

        ])

    }
}
