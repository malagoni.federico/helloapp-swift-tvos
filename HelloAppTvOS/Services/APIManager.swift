//
//  ApiManager.swift
//  HelloAppTvOS
//
//  Created by Federico Malagoni on 20/02/23.
//

import Foundation

struct APIManager {

    public static let shared = APIManager()

    private let decoder = JSONDecoder()

    let baseURL = URL(string: "https://api.open-meteo.com/")!

    public enum APIError: Error {
        case noResponse
        case jsonDecodingError(error: Error)
        case networkError(error: Error)
    }

    public enum Version: String {
        case v1 = "v1"
    }

    public enum Endpoint {
        case forecast

        func path() -> String {
            switch self {
            case .forecast:
                return "forecast"
            }
        }
        func version() -> String {
            return Version.v1.rawValue
        }
    }

    public func REQUEST<T: Codable>(endpoint: Endpoint,
                                    params: [String: Any]?,
                                    completionHandler: @escaping (Result<T, APIError>) -> Void) {
        let version = endpoint.version()
        let path = endpoint.path()

        let queryURL = baseURL.appendingPathComponent(version + "/" + path)
        var components = URLComponents(url: queryURL, resolvingAgainstBaseURL: true)!
        components.queryItems = []
        if let params = params {
            for value in params {
                components.queryItems?.append(URLQueryItem(name: value.key,
                                                           value: String(describing: value.value)))
            }
        }
        var request = URLRequest(url: components.url!)
        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { (data, _, error) in
            guard let data = data else {
                DispatchQueue.main.async {
                    completionHandler(.failure(.noResponse))
                }
                return
            }
            guard error == nil else {
                DispatchQueue.main.async {
                    completionHandler(.failure(.networkError(error: error!)))
                }
                return
            }
            do {
                let object = try self.decoder.decode(T.self, from: data)
                DispatchQueue.main.async {
                    completionHandler(.success(object))
                }
            } catch let error {
                DispatchQueue.main.async {
                    #if DEBUG
                    print("JSON Decoding Error: \(error)")
                    #endif
                    completionHandler(.failure(.jsonDecodingError(error: error)))
                }
            }
        }
        task.resume()
    }
}
