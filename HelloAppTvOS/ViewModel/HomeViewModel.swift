//
//  HomeViewModel.swift
//  HelloAppTvOS
//
//  Created by Federico Malagoni on 21/02/23.
//

import UIKit
import CoreLocation

// MARK: Define here actions that is needed
protocol HomeViewModelHandler {
    var itemsRefresh: (() -> Void)? { get set }
}
class HomeViewModel: HomeViewModelHandler {
    var itemsRefresh: (() -> Void)?

    var item: WeatherDTO? {
        didSet {
            itemsRefresh?()
        }
    }

    enum LocationType {
        case home
        var location: CLLocation {
            switch self {
            case .home:
                return CLLocation.init(latitude: 52.52, longitude: 13.41)
            }
        }
    }

    internal init(locationType: LocationType) {

        fetchForLocation(location: locationType.location)
    }

    func fetchForLocation(location: CLLocation) {
        let params: [String: Any] = [
            "latitude": location.coordinate.latitude,
            "longitude": location.coordinate.longitude,
            "hourly": "temperature_2m"
        ]

        APIManager.shared.REQUEST(endpoint: .forecast, params: params) { (result: Result<WeatherDTO, APIManager.APIError>) in
            switch result {
            case .success(let response):
                DispatchQueue.main.async {
                    self.item = response
                }
            case .failure(let error):
                print(error)
            }
        }

    }

}
